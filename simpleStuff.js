function doStuff(a,b) {
    console.log(a + " + " + b);
    return a+b;
}

const runFunction = doStuff;
runFunction(4,5);

function checkoutCallbackUsage(a, b, callback) {
    console.log("test: " + a + " und " + b);
    callback;
}

checkoutCallbackUsage(4,5, doStuff(6,8));
checkoutCallbackUsage(8,9,runFunction(8,9))