function doStuff(data){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if(typeof(data) == 'number') {
                console.log(data);
                resolve(data * data);
            }
            else {
                reject(new Error("an error occured"));
            }
        }, 1000);
    });
}

doStuff(2)
    .then((firstResult) => {
        console.log(firstResult);
        return doStuff(firstResult);
    })
    .then((secondResult) => {
        console.log(secondResult);
        return doStuff('secondResult');
    })
    .then((thirdResult) => {
        console.log(thirdResult);
    })
    .catch ((error) => {
        console.log(error.message);
    })