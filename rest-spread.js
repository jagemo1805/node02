function add(a,b) {
    return a + b;
}

console.log(add(8,9));

// rest operator - converts a list of values into an iterable array
function addValuesOfAListWithRestOperator(a,...b) {
    let sum = a;
    b.forEach((num) => { sum += num })
    return sum;
}
console.log(addValuesOfAListWithRestOperator(2,3,4));

// spread operator - spreads an array over e.g. arguments
const arr = [2,3,9];

function addArrayValuesViaSpreadOperator(...a) {
    let sum = 0;
    a.forEach( num => sum += num );
    return sum;
}

console.log(addArrayValuesViaSpreadOperator(...arr));
