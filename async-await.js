function doStuff(data){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if(typeof(data) == 'number') {
                // console.log(data);
                resolve(data * data);
            }
            else {
                reject(new Error("an error occured"));
            }
        }, 1000);
    });
}

// always returns a promise, in this case doStuff is called via await and the variable will be returned 
async function chainStuff() {
    let result = await doStuff(2);
    let bResult = await doStuff(result);
    let cResult = await doStuff(bResult);
    return cResult;
}

chainStuff()
.then((result) => {
    console.log(result);
})
.catch((error) => {
    console.log(error.message);
})